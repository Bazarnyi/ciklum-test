﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace CiklumTest
{
    class PinterestTest
    {
        IWebDriver driver;

        public void FBLogin()
        {
            driver.Url = "https://facebook.com";
            driver.FindElement(By.XPath(".//*[@id='email']")).SendKeys("test.den911@gmail.com");
            driver.FindElement(By.XPath(".//*[@id='pass']")).SendKeys("denys1986");
            driver.FindElement(By.XPath(".//*[@id='loginbutton']/input")).Click();

            driver.FindElement(By.XPath(".//*[text()='Tester Denys']"));
        }

        public void PinterestFBLogin()
        {
            driver.Navigate().GoToUrl("https://www.pinterest.com/");
            driver.FindElement(By.XPath(".//button[contains(@class, 'Facebook')]")).Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(50));
        }

        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
        }

        [Test]
        public void FacebookLogin()
        {
            FBLogin();
            PinterestFBLogin();

            driver.FindElement(By.XPath(".//div[@class='usernameLink']/span[text()='Tester']"));
        }

        [Test]
        public void TestingSearch()
        {
            FBLogin();
            PinterestFBLogin();

            IWebElement search = driver.FindElement(By.XPath(".//input[@class='Input Module field']"));

            search.SendKeys("Adriana Lima");
            search.SendKeys(Keys.Return);

            IList<IWebElement> SearchResults = driver.FindElements(By.XPath(".//div[@class='Module SearchPageContent']/div/div[1]/*"));
            int Number = SearchResults.Count;

            IWebElement Text = driver.FindElement(By.XPath(".//div[@class='Module SearchPageContent']/div/div[1]/div[1]/div/div/div[@class='pinMetaWrapper']/div/p[contains (text(),'Adriana Lima')]"));

        }

        [TearDown]
        public void EndTest()
        {
            driver.Quit();
        }
    }
}